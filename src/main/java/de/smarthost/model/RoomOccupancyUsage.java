package de.smarthost.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoomOccupancyUsage {

	private Integer premiumRooms = 0;
	private Integer economyRooms = 0;
	private Integer premiumAmount = 0;
	private Integer economyAmount = 0;

	@Override
	public String toString() {
		return String.format("PremiumRooms %d , EconomyRooms %d -> PremiumAmount %d, EconomyAmount %d", premiumRooms,
				economyRooms, premiumAmount, economyAmount);
	}
}
