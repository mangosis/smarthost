package de.smarthost.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.smarthost.rest.handler.RoomOptHandler;
import de.smarthost.rest.response.RoomOccupancyResponse;

@RestController
@RequestMapping(value = "/v1/room")
public class RoomOptController {

	@Autowired
	private RoomOptHandler roomOptHandler;


	@PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	//All fields are required, user cannot pass empty or null to the required fields
	public ResponseEntity<RoomOccupancyResponse> getById(@RequestParam Integer freePremiumRooms,
			@RequestParam Integer freeEconomyRooms, @RequestParam List<Integer> potentialGuestsBudgetList) {
		return roomOptHandler.getRoomOccupanceyResponse(freePremiumRooms, freeEconomyRooms, potentialGuestsBudgetList);
	}

}
