package de.smarthost.rest.handler;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import de.smarthost.model.RoomOccupancyUsage;
import de.smarthost.rest.response.RoomOccupancyResponse;
import de.smarthost.service.RoomOptService;

@Component
public class RoomOptHandler {

	private final RoomOptService roomService;

	public RoomOptHandler(final RoomOptService roomService) {
		this.roomService = roomService;
	}

	public ResponseEntity<RoomOccupancyResponse> getRoomOccupanceyResponse(final Integer freePremiumRooms,
			final Integer freeEconomyRooms, final List<Integer> potentialGuestsBudgetList) {

		RoomOccupancyUsage roomUsage = roomService.calculateRoomOccupancey(freeEconomyRooms, freePremiumRooms,
				potentialGuestsBudgetList);

		RoomOccupancyResponse roomOccupanceyResponse = RoomOccupancyResponse.builder()
				.economyRooms(roomUsage.getEconomyRooms()).economyAmount(roomUsage.getEconomyAmount())
				.premiumRooms(roomUsage.getPremiumRooms()).premiumAmount(roomUsage.getPremiumAmount()).build();

		return ResponseEntity.ok(roomOccupanceyResponse);
	}

}
