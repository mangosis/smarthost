package de.smarthost.rest.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class RoomOccupancyResponse {
	@Builder.Default
	private int premiumRooms = 0;
	@Builder.Default
	private int economyRooms = 0;
	@Builder.Default
	private double premiumAmount = 0.0;
	@Builder.Default
	private double economyAmount = 0.0;
}
