package de.smarthost.service;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import de.smarthost.model.RoomOccupancyUsage;

@Service
public class RoomOptService {

	// Minimum Premium Budget
	final static Integer MBB = 100;

	public RoomOccupancyUsage calculateRoomOccupancey(Integer freePremiumRooms, Integer freeEconomyRooms,
			final List<Integer> potentialGuestsBudgetList) {
		Collections.sort(potentialGuestsBudgetList, Collections.reverseOrder());

		final boolean enoughEconomyRooms = potentialGuestsBudgetList.parallelStream().filter(budget -> budget < 100)
				.count() < freeEconomyRooms;

		/*
		 * pRooms: Number of recommended premium rooms. 
		 * eRooms: Number of recommended economy rooms. 
		 * pMoney: Total money can be made from premium guests.   
		 * eMoney: Total money can be made from economy guests.
		 */

		int pRooms = 0, eRooms = 0, pAmount = 0, eAmount = 0;

		for (Integer cBudget : potentialGuestsBudgetList) {

			if (cBudget >= MBB && freePremiumRooms > 0) {
				pRooms++;
				pAmount += cBudget;
				freePremiumRooms--;
			} else if (cBudget < MBB) {
				if (freeEconomyRooms > 0 && enoughEconomyRooms) {
					++eRooms;
					eAmount += cBudget;
					freeEconomyRooms--;
				} else if (freePremiumRooms > 0 && !enoughEconomyRooms) {
					pRooms++;
					pAmount += cBudget;
					freePremiumRooms--;
				} else if (freeEconomyRooms > 0) {
					++eRooms;
					eAmount += cBudget;
					freeEconomyRooms--;
				} else if (freePremiumRooms > 0) {
					pRooms++;
					pAmount += cBudget;
					freePremiumRooms--;
				}
			}

		}

		return new RoomOccupancyUsage(pRooms, eRooms, pAmount, eAmount);

	}

}
