package de.smarthost.test;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import de.smarthost.model.RoomOccupancyUsage;
import de.smarthost.service.RoomOptService;

@SpringBootTest
public class RoomOccupancyOptimizationTest {

	@Autowired
	private RoomOptService roomOptService;

	private final String _msg_premium_room = "Expected Premium Rooms: %d";
	private final String _msg_premium_amount = "Expected Premium Amount: %d";
	private final String _msg_economy_room = "Expected Economy Rooms: %d";
	private final String _msg_economy_amount = "Expected Economy amount: %d";

	@Test
	public void test1() {
		final List<Integer> potentialGuestsBudgetList = Arrays.asList(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);
		RoomOccupancyUsage roomOccupanceyUsage = roomOptService.calculateRoomOccupancey(3, 3, potentialGuestsBudgetList);
		Assert.isTrue(roomOccupanceyUsage.getPremiumRooms() == 3, String.format(_msg_premium_room, 3));
		Assert.isTrue(roomOccupanceyUsage.getEconomyRooms() == 3, String.format(_msg_economy_room, 3));
		Assert.isTrue(roomOccupanceyUsage.getPremiumAmount() == 738, String.format(_msg_premium_amount, 738));
		Assert.isTrue(roomOccupanceyUsage.getEconomyAmount() == 167, String.format(_msg_economy_amount, 167));
	}

	@Test
	public void test2() {
		final List<Integer> potentialGuestsBudgetList = Arrays.asList(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);
		RoomOccupancyUsage roomOccupanceyUsage = roomOptService.calculateRoomOccupancey(7, 5, potentialGuestsBudgetList);
		Assert.isTrue(roomOccupanceyUsage.getPremiumRooms() == 6, String.format(_msg_premium_room, 6));
		Assert.isTrue(roomOccupanceyUsage.getEconomyRooms() == 4, String.format(_msg_economy_room, 4));
		Assert.isTrue(roomOccupanceyUsage.getPremiumAmount() == 1054, String.format(_msg_premium_amount, 1054));
		Assert.isTrue(roomOccupanceyUsage.getEconomyAmount() == 189, String.format(_msg_economy_amount, 189));
	}

	@Test
	public void test3() {
		final List<Integer> potentialGuestsBudgetList = Arrays.asList(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);
		RoomOccupancyUsage roomOccupanceyUsage = roomOptService.calculateRoomOccupancey(2, 7, potentialGuestsBudgetList);
		Assert.isTrue(roomOccupanceyUsage.getPremiumRooms() == 2, String.format(_msg_premium_room, 2));
		Assert.isTrue(roomOccupanceyUsage.getEconomyRooms() == 4, String.format(_msg_economy_room, 4));
		Assert.isTrue(roomOccupanceyUsage.getPremiumAmount() == 583, String.format(_msg_premium_amount, 583));
		Assert.isTrue(roomOccupanceyUsage.getEconomyAmount() == 189, String.format(_msg_economy_amount, 189));
	}

	@Test
	public void test4() {
		final List<Integer> potentialGuestsBudgetList = Arrays.asList(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);
		RoomOccupancyUsage roomOccupanceyUsage = roomOptService.calculateRoomOccupancey(7, 1, potentialGuestsBudgetList);
		Assert.isTrue(roomOccupanceyUsage.getPremiumRooms() == 7, String.format(_msg_premium_room, 7));
		Assert.isTrue(roomOccupanceyUsage.getEconomyRooms() == 1, String.format(_msg_economy_room, 1));
		Assert.isTrue(roomOccupanceyUsage.getPremiumAmount() == 1153, String.format(_msg_premium_amount, 1153));
		Assert.isTrue(roomOccupanceyUsage.getEconomyAmount() == 45, String.format(_msg_premium_room, 45));
	}
	
	@Test
	public void test5() {
		final List<Integer> potentialGuestsBudgetList = Arrays.asList(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);
		RoomOccupancyUsage roomOccupanceyUsage = roomOptService.calculateRoomOccupancey(1, 1, potentialGuestsBudgetList);
		Assert.isTrue(roomOccupanceyUsage.getPremiumRooms() == 1, String.format(_msg_premium_room, 1));
		Assert.isTrue(roomOccupanceyUsage.getEconomyRooms() == 1, String.format(_msg_economy_room, 1));
		Assert.isTrue(roomOccupanceyUsage.getPremiumAmount() == 374, String.format(_msg_premium_amount, 374));
		Assert.isTrue(roomOccupanceyUsage.getEconomyAmount() == 99, String.format(_msg_premium_room, 99));
	}
	
	@Test
	public void test6() {
		final List<Integer> potentialGuestsBudgetList = Arrays.asList(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);
		RoomOccupancyUsage roomOccupanceyUsage = roomOptService.calculateRoomOccupancey(0, 0, potentialGuestsBudgetList);
		Assert.isTrue(roomOccupanceyUsage.getPremiumRooms() == 0, String.format(_msg_premium_room, 0));
		Assert.isTrue(roomOccupanceyUsage.getEconomyRooms() == 0, String.format(_msg_economy_room, 0));
		Assert.isTrue(roomOccupanceyUsage.getPremiumAmount() == 0, String.format(_msg_premium_amount, 0));
		Assert.isTrue(roomOccupanceyUsage.getEconomyAmount() == 0, String.format(_msg_premium_room, 0));
	}

}
