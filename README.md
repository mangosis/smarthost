# README #

This README helps to compile, test and run the code

## Project ##
Room Occupancy Service

### What is this repository for? ###
* Coding challenge

### How do I get set up? ###

* Install Java 11
* Install Maven (Latest)
* Lombok setup please visit [https://projectlombok.org/setup/overview]

### Build, Test and Run ###

* mvn package
* java -jar room-occupancy-service-1.0.0-SNAPSHOT.jar
* Test with Swagger on your browser [http://localhost:9999/swagger-ui.html]
 

### Who do I talk to? ###

* mysialkot@hotmail.com